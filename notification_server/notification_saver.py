from datetime import datetime
import os


class NotificationSaver:
    @staticmethod
    def create_folders_if_needed(path):
        os.makedirs(path, exist_ok=True)

    @staticmethod
    def get_date_folder(base_dir='notifications'):
        date = str(datetime.now().date())
        return os.path.join(base_dir, date)

    @staticmethod
    def save_notification(base_dir, title, message, timestamp) -> bool:
        dirname = NotificationSaver.get_date_folder(base_dir)
        filename = f'{title}_{timestamp}.md'

        full_file_name = os.path.join(dirname, filename)
        full_folder_name = os.path.dirname(full_file_name)
        NotificationSaver.create_folders_if_needed(full_folder_name)

        with open(full_file_name, 'w') as file:
            file.write(message)

