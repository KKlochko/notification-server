###############################################################################
# Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>             #
#                                                                             #
# This file is part of notification-server.                                   #
#                                                                             #
# notification-server is free software: you can redistribute it and/or modify #
# it under the terms of the GNU Affero General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or (at your  #
# option) any later version.                                                  #
#                                                                             #
# notification-server is distributed in the hope that it will be useful, but  #
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY  #
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public      #
# License for more details.                                                   #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with notification-server. If not, see <https://www.gnu.org/licenses/>.#
###############################################################################

import asyncio
import sys
from rich.console import Console

import typer
from notification_server.server import Server

cli_app = typer.Typer(rich_markup_mode="rich")
notify = typer.Typer()


@cli_app.command()
def up(
    ip: str = typer.Option(
        "127.0.0.1", "--ip", "-i",
        help="The server's ip.",
        rich_help_panel="Connection",
    ),
    port: int = typer.Option(
        5555, "--port", "-p",
        help="The server's port to notify [b]clients[/].",
        rich_help_panel="Connection",
    ),
    producer_port: int = typer.Option(
        5554, "--producer-port", "-l",
        help="The server's port to listen [b]producers[/].",
        rich_help_panel="Connection",
    ),
    notification_dir: str = typer.Option(
        None, "--notification-dir", "-d",
        help="The server will save notifications to the directory by the path. Does not save if this option is not provided.",
        rich_help_panel="Persistence",
        show_default=False,
    )
):
    """
    This command [b]start[/] the [yellow b]server[/] which receives notifications and sends to the clients.
    """

    try:
        server = Server(ip, port, producer_port, notification_dir)
        asyncio.run(server.up())
    except KeyboardInterrupt:
        console = Console()
        console.print('[yellow b][INFO][/] [green b]bye[/]')
        sys.exit(0)
