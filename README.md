# notification-server

This is a notification server. It gets a notification from a producer and sends to clients.

# Ecosystem
- [notification-server](https://gitlab.com/KKlochko/notification-server) (current)
- [notification-producer](https://gitlab.com/KKlochko/notification-producer)
- [notification-client](https://gitlab.com/KKlochko/notification-client)

# Setup

- Clone this repository.
- Ensure that python and pip are installed:
  ```
  python --version
  python -m pip -V
  # or
  python3 --version
  python3 -m pip -V
  ```

- Create a virtual environment if needed:
  Using `virtualenv`:
  ```
  virtualenv .venv
  . .venv/bin/activate
  # or
  virtualenv3 .venv
  . .venv/bin/activate
  ```

  Using `poetry`:
  ```
  poetry shell
  ```

- Install the dependencies:
  Using `pip`:
  ```
  pip install -r requirements.txt
  # or
  pip3 install -r requirements.txt
  ```

  Using `poetry`:
  ```
  poetry install
  ```

- Run the main file:
  Use options --ip, --port and --producer-port to change the server address.
  127.0.0.1:5555 is the default address for the clients.
  127.0.0.1:5554 is the default address for the producers.
  ```
  python -m notification_server.main --help
  # or
  python3 -m notification_server.main --help

  ```

# Commands

- Start the server:
  ```
  python -m notification_producer.main
  python -m notification_server.main --ip 0.0.0.0 --port 4000 --producer-port 5000
  # or
  python3 -m notification_producer.main
  python3 -m notification_server.main --ip 0.0.0.0 --port 4000 --producer-port 5000
  ```

# Author

Kostiantyn Klochko (c) 2023

# License

Under the GNU Affero General Public License v3.0 or later.

